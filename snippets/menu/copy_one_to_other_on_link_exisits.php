<?php
/**
 * Копирование пунктов из одного меню в другое (одного типа)
 * при условии отсуствия ссылки на директорию в итоговом меню
 *
 * /project_public_directory/.top.menu.php => /.top.menu.php
 *
 * добавим пункты из /project_public_directory/.top.menu.php в /.top.menu.php если в последнем нет пункта со ссылкой
 * на директорию /project_public_directory/
 */

// ... Подключение ядра Битрикса

\Bitrix\Main\Loader::includeModule('fileman'); // для работы с меню

$path = '/project_public_directory/'; // путь к директории которую ищем в меню
$menuId = 'top'; // тип меню

// получим путь к публичной директории и убедимся в наличии закрывающего слеша
// @FIXME: use Path class
$workDirectoryPath = rtrim($path, '\\/') . DIRECTORY_SEPARATOR;


// получим путь к корневому меню указаннго типа
// @FIXME: use Path class
$menuPath = $_SERVER['DOCUMENT_ROOT'] . '/.' . $menuId . '.menu.php';

$exists = false; // наличие пункта меню
if(Bitrix\Main\IO\File::isFileExists($menuPath))
{
    $topMenu = \CFileMan::GetMenuArray($menuPath);
    // проверим наличие такого пункта (сравним по ссылке)
    foreach($topMenu["aMenuLinks"] as $menuItem)
    {
        if($menuItem[1] === $workDirectoryPath)
        {
            $exists = true;
        }
    }
}
else
{
    $topMenu["aMenuLinks"] = [];
    // CFileMan::SaveMenu создаст меню даже если его нет
}

if(!$exists)
{
    // добавим копированием все пункты меню в итоговое меню если копируемое меню существует и содержит пункты меню
    $tmpMenuPath = $_SERVER['DOCUMENT_ROOT'] . $workDirectoryPath . '/.' . $menuId . '.menu.php';
    // @FIXME: use Path class
    if(Bitrix\Main\IO\File::isFileExists($tmpMenuPath))
    {
        $tmpMenu = \CFileMan::GetMenuArray($tmpMenuPath);
        if(is_array($tmpMenu["aMenuLinks"]) && count($tmpMenu["aMenuLinks"]) > 0)
        {
            // на всякий случай, приведем к массиву то что хотим объединить, а то вдруг \CFileMan::GetMenuArray вернул нам null или пустую строку
            $topMenu["aMenuLinks"] = array_merge((array)$topMenu["aMenuLinks"], $tmpMenu["aMenuLinks"]);

            \CFileMan::SaveMenu('/.top.menu.php', $topMenu["aMenuLinks"]);
        }
    }
}