<?php

$iblockTypeId = 'lri';

// region Импорт инфоблоков

// массив файлов для импорта
// порядок важен если используются ссылки на инфоблоки, они должны быть импортированы первыми
$arImportFiles = [
    '/local/modules/cbit.lineroundinspection/install/import/lri/line_round_inspection.xml',
    '/local/modules/cbit.lineroundinspection/install/import/lri/lri_checkpoint.xml',
    '/local/modules/cbit.lineroundinspection/install/import/lri/lri_object.xml',
    '/local/modules/cbit.lineroundinspection/install/import/lri/lri_status.xml',
    '/local/modules/cbit.lineroundinspection/install/import/lri/lri_tag.xml',
];

foreach ($arImportFiles as $filePath)
{
    // создадим объект файла
    $file = new \Bitrix\Main\IO\File($_SERVER['DOCUMENT_ROOT'] . $filePath);

    if(!\Bitrix\Main\IO\File::isFileExists($_SERVER['DOCUMENT_ROOT'] . $filePath))
    {
        // TODO: вывести ошибку если файл не найден
        break;
    }

    // в данном случае мы хотим чтобы метод вернул нам идентификатор созданного или сущестующего инфоблока
    $iblockId = \ImportXMLFile(
        $_SERVER['DOCUMENT_ROOT'] . $filePath, // абсолютный путь к файлу
        $iblockTypeId, // идентификатор типа инфоблока, иначе будет загружен в тип "Каталог 1С" указанный в настройках битрикса
        '', 'D', 'D', false, false, false, false, true
    );

    // если вернулось не число (так как мы запросили идентификатор в методе)
    if(!is_integer($iblockId))
    {
        // TODO: вывести ошибку - не удалось импортировать инфоблок
        break;
    }

    echo  'Инфоблок <samp>' . $file->getName() . '</samp> импортирован и определен как #' . $iblockId . '. ';
}

