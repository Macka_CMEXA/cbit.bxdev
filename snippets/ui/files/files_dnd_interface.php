<?php
/**
 * Интерфейс вывода/добавления файлов на основе прав доступа (чтение/добавление)
 *
 * @global \CMain $APPLICATION
 * @var boolean $canEdit - Разрешено ли изменять
 * @var string $inputName - Название поля ввода
 * @var array $fileIds - Массив {@see \CFile идентификаторов файлов }
 * @var int|string $elementId - Идентификатор элемента инфоблока
 * @var int|string $iblockId - Идентификатор инфоблока
 */


$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if ($request->isPost() && check_bitrix_sessid())
{
    $postList = $this->request->getPostList();

    $files = $postList->get($inputName);
    $filesDel = (array)$postList->get($inputName.'_del');

    // Файлы уже сохранены
    // массив $files содержит идентификаторы файлов
    // массив $filesDel содержит идентификаторы удаленных файлов файлов


    // Сохранение в свойство инфоблока c обновлением ссылок на файлы
    \Bitrix\Main\Loader::includeModule('iblock');
    $arFiles = [];
    if(is_array($files))
    {
        foreach ($files as $key => $fileId)
        {
            if (in_array($fileId, $filesDel))
            {
                $arFile = \CFile::MakeFileArray($fileId);
                $arFile['del'] = 'Y';
            }
            else
            {
                $arFile = \CFile::MakeFileArray($fileId);
            }
            $arFiles[] = $arFile;
        }
    }
    \CIBlockElement::SetPropertyValuesEx($elementId, $iblockId, [strtoupper($inputName) => $arFiles]);

    // TODO: Сохранение в свойство инфоблока c сохраннием ссылок
    // ...
}
?>

<form method="post" action="<?=POST_FORM_ACTION_URI?>">
    <?=bitrix_sessid_post()?>

    <div class="form-group">
        <label for="file_input_mfi<?=$inputName?>">Прикрепленные файлы:</label>
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.file.input",
            "gpn_file",
            Array(
                "ALLOW_UPLOAD" => "A",
                "ALLOW_UPLOAD_EXT" => "",
                "INPUT_NAME" => $inputName,
                "INPUT_VALUE" => $fileIds,
                "MAX_FILE_SIZE" => "",
                "MODULE_ID" => "iblock",
                "MULTIPLE" => "Y",
                "IS_EDIT" => $canEdit,
            )
        );?>
    </div>
    <input type="submit" name="submit" value="Submit">
</form>