<?php
/**
 * Блок оповещения об ошибке, в том числе о режиме администратора
 *
 * @var array $arErrors - Массив сообщений с ошибками
 * @var boolean $isAdminMode - Состояние режима администратора
 */

\Bitrix\Main\UI\Extension::load(["ui.alerts"]);
?>
<style>
    .hidden { display: none; }
</style>

<?
$classList = [];
$messages = [];
if(is_array($arErrors) && count($arErrors) > 0)
{
    $classList[] = 'ui-alert-danger';
    $classList[] = 'error';
    $messages = $arErrors;
}
elseif($isAdminMode)
{
    $classList[] = 'ui-alert-warning';
    $messages[] = 'Внимание! Вы находитесь в режиме администратора и можете видеть и изменять данные, доступ к которым ограничен!';
}
else
{
    $classList[] = 'hidden';
}

?>

<div class="ui-alert <?=implode(' ', $classList)?>" >
    <span class="ui-alert-message"><?=implode(' ', $messages)?>"</span>
</div>