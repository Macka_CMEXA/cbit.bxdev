<?php

$iblockTypeId = 'lri';

// region Добавление нового типа инфоблока

// добавим языковые сообщения (на примере RU, можно добавить и для других языков так же)
$langTypeAR = \Bitrix\Iblock\TypeLanguageTable::add([
    'IBLOCK_TYPE_ID' => $iblockTypeId,
    'LANGUAGE_ID'    => 'ru', // идентификатор языка
    'NAME'           => 'Линейные обходы', // Название типа
    'SECTION_NAME'   => 'Раздел', // Название раздела
    'ELEMENT_NAME'   => 'Справочник' // Название элемента
]);

$arFields = [
    'ID'           => $iblockTypeId, // строковый идентификатор типа инфоблока
    'SECTIONS'     => 'N', // содержит разделы
    'IN_RSS'       => 'N', // включить RSS ленту
    'SORT'         => 100,
    // требует объект? хотя на самом деле отсюда нужен только IBLOCK_TYPE_ID
    'LANG_MESSAGE' => \Bitrix\Iblock\TypeLanguageTable::getByPrimary($langTypeAR->getPrimary())->fetchObject()
];

try
{
    $result = \Bitrix\Iblock\TypeTable::add($arFields);
}
catch(\Exception $e)
{
    // удалим созданные языковые сообщения для типа инфоблока в случае ошибки создания типа
    \Bitrix\Iblock\TypeLanguageTable::deleteByIblockTypeId($iblockTypeId);
    throw new \Exception($e->getMessage()); // повторим исключение
}

if($result->isSuccess())
{
    // сбросим управляемый кеш типов
    \Bitrix\Main\Application::getInstance()->getManagedCache()->cleanDir('b_iblock_type');
}

// endregion

// region Выборка типов инфоблоков

// region Выбрать конкретный тип и его имя на определенном языке
$row = \Bitrix\Iblock\TypeTable::getRow([
    'select' => ['ID', 'NAME' => 'LANG_MESSAGE.NAME'],
    'filter' => ['=LANG_MESSAGE.LANGUAGE_ID' => 'ru', 'ID' => $iblockTypeId]
]);
// endregion

// endregion

// region Удаление типов инфоблоков

// endregion